{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# flexDOAS Tutorial"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "This is a tutorial for the flexDOAS library. The example runs a retrieval of NO$_2$ concentrations from an active LP-DOAS instrument.\n",
    "\n",
    "The tutorial has four steps:\n",
    "1. Read the LP-DOAS measurements\n",
    "2. Write a forward model that implements the DOAS equation.\n",
    "3. Run the DOAS analysis.\n",
    "4. Visualize results"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import matplotlib\n",
    "matplotlib.use('module://ipympl.backend_nbagg') # or: %matplotlib inline\n",
    "\n",
    "import os\n",
    "import numpy as np\n",
    "import xarray\n",
    "\n",
    "import doas"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Read data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The LP-DOAS are stored in two text files that are converted to a `xarray.Dataset`. It is also possible to just open a netCDF file with the data (if available) instead of parsing some text files. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# reference and measurement intensity spectra\n",
    "ref_filename = os.path.join('data', 'lpdoas', \"reference.dat\")\n",
    "spe_filename = os.path.join('data', 'lpdoas', \"spectra.dat\")\n",
    "\n",
    "# read data\n",
    "wavelengths, ref_intensities = np.loadtxt(ref_filename, unpack=True)\n",
    "intensities = np.loadtxt(spe_filename)\n",
    "\n",
    "# create xarray.Dataset\n",
    "n_times, n_bands = intensities.shape\n",
    "\n",
    "input_data = xarray.Dataset()\n",
    "\n",
    "input_data['band'] = xarray.DataArray(np.arange(n_bands), dims='band')\n",
    "input_data['time'] = xarray.DataArray(np.arange(n_times), dims='time')\n",
    "\n",
    "input_data['wavelength'] = xarray.DataArray(wavelengths, dims='band')\n",
    "input_data['intensity'] = xarray.DataArray(intensities, dims=('time', 'band'))\n",
    "input_data['ref_intensity'] = xarray.DataArray(ref_intensities, dims='band')\n",
    "\n",
    "input_data"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Forward model (DOAS equation)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "class StandardDOAS:\n",
    "    def __init__(self, mapping=None, poly=None, shift=None, xss=None):\n",
    "        \"\"\"\\\n",
    "        Standard DOAS function for that can be used with the flexDOAS library.\n",
    "\n",
    "        Parameter\n",
    "        =========\n",
    "        poly     broad-band approximation (doas.SplineDefinition)\n",
    "        xss      list of cross sections\n",
    "        shift    shift between ref/spe and cross sections\n",
    "\n",
    "        mapping  from input dataset to variable names in this class (dict)\n",
    "        \"\"\"\n",
    "\n",
    "        # build model\n",
    "        self.len_state_vector = 0\n",
    "        self.mapping = mapping\n",
    "        \n",
    "        # Cross section\n",
    "        xss_functions = []\n",
    "        for xs in xss:\n",
    "            self.xss_wavelength = xs['wavelength']\n",
    "            xss_functions.append(\n",
    "                doas.CrossSection.from_xs_file(start=self.len_state_vector, **xs)\n",
    "            )\n",
    "            self.len_state_vector +=1\n",
    "\n",
    "        self.xss = doas.FunctionsList('XSS', xss_functions)\n",
    "\n",
    "        # Polynomial\n",
    "        self.poly = doas.Spline.from_definition(sdef=poly, start=self.len_state_vector)\n",
    "        self.len_state_vector += self.poly.n_coefficients\n",
    "\n",
    "        # Spectral shift\n",
    "        if shift is None:\n",
    "            self.shift = None\n",
    "        else:\n",
    "            self.shift = doas.Spline.from_definition(sdef=shift, start=self.len_state_vector)\n",
    "            self.len_state_vector += self.shift.n_coefficients\n",
    "\n",
    "\n",
    "    def __call__(self, x, b):\n",
    "        \"\"\"\n",
    "        This function is called by the retrieval algorithm. It should return a vector that is squared and minimized.\n",
    "        \"\"\"\n",
    "        # get center wavelengths, spectrum and reference from parameter vector (b)\n",
    "        cw = np.array(b['cw'])\n",
    "        spe = np.array(b['spe'])\n",
    "        ref = np.array(b['ref'])\n",
    "\n",
    "        # shift function\n",
    "        if self.shift is None:\n",
    "            shift = 0.0\n",
    "        else:\n",
    "            shift = self.shift(x, b)\n",
    "\n",
    "        # absorption cross sections\n",
    "        tau = np.sum(self.xss(x, b), axis=0)\n",
    "        tau = doas.misc.interpolate(cw+shift, self.xss_wavelength, tau, k=3)\n",
    "        \n",
    "        # DOAS equation\n",
    "        chi = np.log(ref) - np.log(spe) - self.poly(x) - tau\n",
    "        \n",
    "        return chi\n",
    "        \n",
    "    \n",
    "    def get_starting_vector(self, b):\n",
    "        \"\"\"\n",
    "        The retrieval algorithm uses this method to get the start vector for the optimization.\n",
    "        \"\"\"\n",
    "        x0 = np.ones(self.len_state_vector)\n",
    "        \n",
    "        if self.shift is not None:\n",
    "            x0[self.shift.mapping] = 0.0\n",
    "        \n",
    "        return x0\n",
    "        "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Definition of a polynomial that is passed to the StandardDOAS class\n",
    "poly = doas.SplineDefinition(name='POLY', kind='cardinal', degree=5,\n",
    "                             bands_or_size=input_data.band.size)\n",
    "\n",
    "# A list of cross sections, which will be convolved with Gauss function with FWHM and interpolate to wavelengths\n",
    "w = np.arange(430,470,0.1)\n",
    "fwhm = np.full(w.shape, 0.67025)\n",
    "\n",
    "xss = [{\n",
    "    'name': 'NO2',\n",
    "    'filename': os.path.join('data', 'lpdoas', 'NO2_Vandaele_2002_294K_400-600nm-0.01nm-vac.dat'),\n",
    "    'filetype': 'ascii',\n",
    "    'wavelength': w, 'fwhm': fwhm,\n",
    "    'scaling': 'ptp', 'highpass': False\n",
    "},{\n",
    "    'name': 'O4',\n",
    "    'filename': os.path.join('data', 'lpdoas', 'O4_ThalmanVolkamer_2013_293K_400-600nm-0.01nm-vac.dat'),\n",
    "    'filetype': 'ascii',\n",
    "    'wavelength': w, 'fwhm': fwhm,\n",
    "    'scaling': 'ptp', 'highpass': False\n",
    "},{\n",
    "    'name': 'O3',\n",
    "    'filename': os.path.join('data', 'lpdoas', 'O3_Serdyuchenko_2014_293K_400-600nm-0.01nm-vac.dat'),\n",
    "    'filetype': 'ascii',\n",
    "    'wavelength': w, 'fwhm': fwhm,\n",
    "    'scaling': 'ptp', 'highpass': False\n",
    "},{\n",
    "    'name': 'H2O',\n",
    "    'filename': os.path.join('data', 'lpdoas', 'H2O_HITRAN2012_400-600nm-0.01nm-vac.dat'),\n",
    "    'filetype': 'ascii',\n",
    "    'wavelength': w, 'fwhm': fwhm,\n",
    "    'scaling': 'ptp', 'highpass': False\n",
    "}]\n",
    "\n",
    "\n",
    "# Definition of a shift function.\n",
    "shift = doas.SplineDefinition(name='SHIFT', kind='cardinal', degree=0,\n",
    "                              bands_or_size=input_data.band.size)\n",
    "\n",
    "# mapping from the names that you use in the forward model and the input dataset\n",
    "mapping = {'cw': 'wavelength', 'ref': 'ref_intensity', 'spe': 'intensity'}\n",
    "\n",
    "forward_model = StandardDOAS(mapping=mapping, poly=poly, shift=shift, xss=xss)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Run retrieval"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "The DOAS analysis is run using the `doas.run_retrieval` function, which requires the `forward_model` and the `input_data`. In addition, `main_dims` is a tuple that defines over which dimensions over which the retrieval loop iterates. The function also requires the `band` to define the results dataset. The `slicing` dictionary can be used to slice the input data, for example, to only run the retrieval for one measurement.\n",
    "\n",
    "The function returns an `xarray.Dataset` with the state vector `x`, its uncertainty `x_std`, the error covariance matrix `S`, the Jacobian `K`, the residual, the root mean square `rms`, the number of iterations `n_iter` and the status of the retrieval (currently not implemented).\n",
    "\n",
    "The `xarray.Dataset` can be saved using the `to_netcdf` method. "
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%time\n",
    "\n",
    "# You can process the data in parallel. In this case, 2 processes are sufficient.\n",
    "processes = 2\n",
    "\n",
    "results = doas.run_retrieval(forward_model, input_data, main_dims=('time', ), band_dim='band', \n",
    "                             bands=input_data.band.values, slicings=None,\n",
    "                             processes=processes)\n",
    "\n",
    "results.attrs.update(dict(('%s_scaling' % xs.name, xs.scaling) for xs in\n",
    "                              forward_model.xss.functions))\n",
    "\n",
    "results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Visualize results"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "flexDOAS currently does not include functions for visualizing results, but you can easily write your own function:"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def show_retrieval(time, input_data, forward_model, results, num=1):\n",
    "    \"\"\"\\\n",
    "    Show DOAS fit results for spectrum \"p\".\n",
    "    \"\"\"\n",
    "    input_data = input_data.sel(time=time)\n",
    "    results = results.sel(time=time)\n",
    "    \n",
    "    x = np.array(results['x'])\n",
    "    x_std = np.array(results['x_std'])    \n",
    "    \n",
    "    cw = input_data['wavelength']\n",
    "    ref = input_data['ref_intensity']\n",
    "    spe = input_data['intensity']\n",
    "    \n",
    "    # make plot\n",
    "    plt.close(num)\n",
    "    fig, axes = plt.subplots(3,3, figsize=(11,6), num=num)\n",
    "    axes = axes.flatten()\n",
    "\n",
    "    # ref/spe\n",
    "    axes[0].plot(cw, spe, label='spe')\n",
    "    axes[0].plot(cw, ref, label='ref')\n",
    "    axes[0].legend(loc=0)\n",
    "    axes[0].set_ylim(ymin=0)\n",
    "    \n",
    "    # residual\n",
    "    axes[1].plot(cw, results['residual'])\n",
    "    axes[1].set_title('RMS = %.3g' % results['rms'])\n",
    "    \n",
    "    # poly (w/o tau)\n",
    "    axes[2].set_title('poly (%s, %d)' % (forward_model.poly.kind, forward_model.poly.degree))\n",
    "    axes[2].plot(cw, forward_model.poly(x), 'b-')\n",
    "    axes[2].plot(cw, forward_model.poly(x) + results['residual'], 'r-')\n",
    "\n",
    "    \n",
    "    # cross sections\n",
    "    for i,xs in zip([3,4,5,6], forward_model.xss.functions):\n",
    "\n",
    "        values = doas.misc.interpolate(cw, xs.wavelength, xs(x, {}))\n",
    "\n",
    "        axes[i].plot(cw, values, color='C0')\n",
    "        axes[i].plot(cw, values + results['residual'], color='C1')\n",
    "\n",
    "        cn = xs.scaling * x[xs.mapping]\n",
    "        cn_err = xs.scaling * x_std[xs.mapping]\n",
    "\n",
    "        axes[i].set_title(r'%s: %s' % (xs.name, doas.misc.slant_column_fmt(cn, cn_err)), loc='right')\n",
    "\n",
    "        \n",
    "    # wavelength shift\n",
    "    axes[7].set_title('shifts (%s, %d)' % (forward_model.shift.kind, forward_model.shift.degree))\n",
    "    axes[7].plot(cw, forward_model.shift(x,{}))\n",
    "\n",
    "    \n",
    "    axes[8].set_visible(False)\n",
    "    \n",
    "    for ax in axes.flatten():\n",
    "        ax.grid(True)\n",
    "\n",
    "    plt.tight_layout()\n",
    "    \n",
    "    \n",
    "show_retrieval(10, input_data, forward_model, results, num=1)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "### Time series of NO$_2$ concentrations"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "Make a time series of the NO$_2$ concentrations."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# compute NO2 column density\n",
    "index = forward_model.xss.get_function_by_name('NO2').mapping\n",
    "\n",
    "no2 = results.NO2_scaling * np.squeeze(results['x'].sel(state_vector=index))\n",
    "no2_std = results.NO2_scaling * np.squeeze(results['x_std'].sel(state_vector=index))\n",
    "\n",
    "# make a quick plot\n",
    "plt.close(2)\n",
    "fig, ax = plt.subplots(num=2)\n",
    "\n",
    "ax.errorbar(results.time, no2, yerr=no2_std, ls='', marker='.', capsize=2)\n",
    "\n",
    "ax.set_xlabel('LP-DOAS spectrum')\n",
    "ax.set_ylabel('NO$_2$ column density [molecules cm$^{-2}$]')\n",
    "ax.set_ylim(0,5e16)\n",
    "ax.grid(True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}

#! /usr/bin/env python
# coding: utf-8

# flexDOAS is a Python library for the development of DOAS
# algorithm.
# Copyright (C) 2016  Gerrit Kuhlmann (gerrit.kuhlmann@empa.ch)
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License a
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import multiprocessing as mp
import logging
import os
import sys
import time

import numpy as np
import scipy
import xarray as xr

from . import cspline
from . import io
from . import misc
from . import solver


logging.basicConfig(level=logging.WARNING)



def iter_dataset(dataset, dims, slicings=None):
    """
    Iterate over indicices along given dims.
    """
    shape = (dataset.dims[d] for d in dims)
    for p in np.ndindex(*shape):
        p = {d: v for d, v in zip(dims, p)}

        if slicings is not None:
            p.update(slicings)

        yield p



def extract_parameter(p, data, mapping):
    parameter = {'p': p}

    for key, var_name in mapping.items():
        parameter[key] = np.asarray(data[p][var_name])

    return parameter



def iter_parameter(data, mapping, main_dims, slicings=None):
    """
    Iterate over dataset pixels and return pixel slice and pixel data.
    """
    for p in iter_dataset(data, main_dims, slicings):
        yield extract_parameter(p, data, mapping)



def print_process(p, main_dims):
    sys.stdout.write('\rProcess: ' + ' x '.join('%5d' % p[dim] for dim in
                                              main_dims))
    sys.stdout.flush()


def run_retrieval(forward_model, dataset, main_dims, band_dim, bands,
                  slicings=None, processes=1, chunksize=1, use_a_priori=False):
    """
    Main function for running a DOAS retrieval.
    """

    if 'p' in forward_model.mapping:
        raise RuntimeError('name `p` in forward_model.mapping is not allowed '
                           'because it is used internally!')


    # init dataset for results
    results  = init_results_dataset(dataset, main_dims,
                                    n_states=forward_model.len_state_vector,
                                    bands=bands,
                                    band_dim=band_dim)

    # create iterator for parameters
    iterator = iter_parameter(dataset, forward_model.mapping, main_dims,
                              slicings=slicings)

    # prepare analysis class for running retrieval
    retrieval = Analysis(forward_model, use_a_priori=use_a_priori,
                         main_dims=main_dims)

    if processes == 1:
        for parameter in iterator:

            p, result = retrieval(parameter)
            p = dict((dim, p[dim]) for dim in main_dims)

            print_process(p, main_dims)

            for key, value in result.items():
                if key in results:
                    results[key][p] = value

        sys.stdout.write('\rrun retrieval done\n')

    else:
        with mp.Pool(processes=processes) as pool:
            for p, result in pool.imap(retrieval, iterator, chunksize=chunksize):

                p = dict((dim, p[dim]) for dim in main_dims)

                print_process(p, main_dims)

                for key, value in result.items():
                    if key in results:
                        results[key][p] = value

        sys.stdout.write('\rrun retrieval done\n')

    return results



class Analysis:
    def __init__(self, forward_model, use_a_priori=False, main_dims=None):
        self.forward_model = forward_model
        self.use_a_priori = use_a_priori

        self.main_dims = main_dims

    def __call__(self, parameter):
        """

        forward_model: callable forward model F(x,b)
        parameter:     the vector 'b' in the forward model as dict
        """
        p = parameter['p']

        results = dict()


        # measurement vector
        y = parameter.get('y', 0.0)
        se = parameter.get('Se', np.full_like(y, np.nan))

        if np.any(np.isnan(y)):
            logging.info('"nan"s in measurement vector (skip)')

            return p, results

        # a priori
        if self.use_a_priori:
            xa, sa = self.forward_model.get_a_priori(parameter)
        else:
            xa, sa = np.nan, np.nan

        x0 = self.forward_model.get_starting_vector(parameter)

        ## get (if available) forward model has method for computing jacobian
        if hasattr(self.forward_model, 'compute_jacobian'):
            fprime = getattr(self.forward_model, 'compute_jacobian', None)
        else:
            fprime = None

        # use gauss-newton algorithm to find solution
        x, results = solver.gauss_newton(x0, y, self.forward_model, se, sa, xa,
                                         b=parameter, fprime=fprime,
                                         max_iters=100)

        if results['success']:
            results['x'] = x
            results['x_std'] = np.sqrt(results['S'].diagonal())

            # compute residual and rms
            results['residual'] = np.array(self.forward_model(x, parameter) - y)
            results['rms'] = np.array(misc.compute_rms(results['residual']))

        else:
            logging.error('DOAS retireval failed: "%s"!' % results['msg'])

            return p, {}

        return p, results



def init_results_dataset(dataset, main_dims, n_states, bands, band_dim='spectral_bands',
                         state_dim='state_vector'):

    # result dataset
    coords = {}
    for dim in main_dims:
        if np.ndim(dataset[dim]) == 0:
            coords[dim] = np.array([dataset[dim]])
        else:
            coords[dim] = dataset[dim]

    coords[state_dim] = np.arange(n_states)
    coords[state_dim+'2'] = np.arange(n_states)

    coords[band_dim] = bands

    data = xr.Dataset(coords=coords)

    # state vector
    dims = main_dims + (state_dim,)
    shape = [data[dim].size for dim in dims]

    data['x'] = xr.DataArray(np.full(shape, np.nan, dtype='f4'), dims=dims)
    data['x_std'] = xr.DataArray(np.full(shape, np.nan, dtype='f4'), dims=dims)

    # a posteriori error covariance matrix
    dims = main_dims + (state_dim, state_dim+'2')
    shape = [data[dim].size for dim in dims]
    data['S'] = xr.DataArray(np.full(shape, np.nan, dtype='f4'), dims=dims,
                             attrs={'long_name': 'a posteriori error'
                                                 ' covariance matrix'})
    # jacobian
    dims = main_dims + (band_dim, state_dim)
    shape = [data[dim].size for dim in dims]
    data['K'] = xr.DataArray(np.full(shape, np.nan, dtype='f4'), dims=dims,
                             attrs={'long_name': 'Jacobian'})
    # residual
    dims = main_dims + (band_dim, )
    shape = [data[dim].size for dim in dims]
    data['residual'] = xr.DataArray(np.full(shape, np.nan, dtype='f4'), dims=dims,
                                    attrs={'long_name': 'residual'})
    # root mean square of fit
    dims = main_dims
    shape = [data[dim].size for dim in dims]
    data['rms'] = xr.DataArray(np.full(shape, np.nan, dtype='f4'), dims=main_dims,
                               attrs={'long_name': 'root mean square of fit'})
    # niter
    data['n_iter'] = xr.DataArray(np.full(shape, -9999, dtype='i4'), dims=main_dims,
                                  attrs={'long_name': 'number of iterations'})
    # success
    data['status'] = xr.DataArray(np.full(shape, -9999, dtype='i4'), dims=main_dims)

    return data



class Function(object):
    name = 'none'
    type_ = 'Function'

    def __repr__(self):
        return '<%s "%s" at %d>' % (self.type_, self.name, id(self))

    def to_netcdf(self, filename, mode='a', group=''):
        """\
        Write Function to netcdf file.
        """
        # write data
        d = self.to_dataset()

        if hasattr(self, 'functions'):
            function_names = np.array([f.name for f in self.functions if f is not None])
            function_types = np.array([f.type_ for f in self.functions if f is not None])

            f = xr.Dataset({
                'function_names': xr.DataArray(function_names, dims='function_dim'),
                'function_types': xr.DataArray(function_types, dims='function_dim')
            })
            d = d.merge(f)

        encoding = dict((k,{'zlib': True}) for k in d)
        d.to_netcdf(filename, mode, group=group, encoding=encoding)

        # writer sub functions
        for function in getattr(self, 'functions', []):
            if function is not None:
                name = '%s/%s' % (group, function.name)
                function.to_netcdf(filename, mode='a', group=name)


    @classmethod
    def from_netcdf(cls, filename, group=''):
        """\
        Create Function from group in netcdf file.
        """
        data = xr.open_dataset(filename, group)

        kwargs = {}
        kwargs.update(data.variables)
        kwargs.update(data.attrs)

        for name,type in zip(
                np.array(kwargs.get('function_names', [])),
                np.array(kwargs.get('function_types', []))
            ):
            function = _name2function.get(type, Function)

            group_name = r'/'.join([group, name])
            kwargs[name] = function.from_netcdf(filename, group_name)


        return cls(**kwargs)




class FunctionsList(Function):
    type_ = "FunctionsList"

    def __init__(self, name, functions=None, **kwargs):
        self.name = name

        if functions is None:
            self.functions = [kwargs[k] for k in np.array(kwargs.get('function_names', []))]
        else:
            self.functions = functions


    def __call__(self, x, b=None):
        if b is None:
            b = {}
        return [f(x, b) for f in self.functions]

    def to_dataset(self):
        return xr.Dataset(
            attrs = {
                'name':    self.name,
            }
        )

    def get_function_by_name(self, name):
        """\
        Returns first occurance of function in list by provided "name". Raises
        ValueError if "name" not in functions.
        """
        for f in self.functions:
            if f.name == name:
                return f
        else:
            raise ValueError('"%s" not functions' % name)

    @property
    def names(self):
        return [f.name for f in self.functions]

    @property
    def mapping(self):
        return np.concatenate([f.mapping for f in self.functions]).flatten()



class CrossSection:

    def __init__(self, name, wavelength, values, scaling=1.0, levels=None, start=0,
        do_caching=False, spline=None, fit_profile=False):
        """\

        fit_profile: if True fit cross section as profile
        """
        self.name = name

        self.values = np.asarray(values)
        self.wavelength = np.asarray(wavelength)
        self.levels = np.asarray(levels)

        if np.ndim(self.levels) == 0:
            self.layer_heights = np.nan
        else:
            self.layer_heights = np.diff(levels)

        self.scaling = scaling

        self.spline = spline
        self.fit_profile = bool(fit_profile)

        if self.fit_profile:
            self.mapping = np.arange(start, start + self.layer_heights.size)
        else:
            if spline is None:
                self.mapping = np.array([start])
            else:
                self.mapping = np.arange(start, start+spline.n_coefficients)

        self.end = self.mapping[-1]

        # use this to cache calculation of tau
        self.do_caching = bool(do_caching)
        self.pointer = None
        self.cached_data = None

        # only use non-zero values for computation
        if np.ndim(self.values) == 2:
            self.nonzero = np.any(self.values != 0.0, axis=0)
            self._nonzero_values2d = self.values[:, self.nonzero]


    @classmethod
    def from_xs_file(cls, filename, filetype, name, start=0,
                     wavelength=None, fwhm=None, scaling=1.0, convolve=True,
                     highpass=None, spline=None, sum_vertically=False,
                     clip=None, **kwargs):
        """\
        Read cross section / optical depth from filename and, if given, convolute
        with FWHM and interpolate to wavelength.
        """
        # read data
        xs_wavelength, xs_values, xs_levels = io.read_cross_section(filename,
                                                                    filetype)

        if clip is not None and np.ndim(xs_values) == 1:
            clip = (clip[0] <= xs_wavelength) & (xs_wavelength < clip[1])

            xs_values = xs_values[clip]
            xs_wavelength = xs_wavelength[clip]

        if filetype == 'arts' and sum_vertically:
            xs_values = np.sum(xs_values, axis=0)
            xs_levels = np.nan

        # interpolate or convolve
        if not convolve and wavelength is None:
            wavelength = xs_wavelength
            values = xs_values
        else:
            if fwhm is None:
                values = misc.interpolate(wavelength, xs_wavelength, xs_values,
                                          k=1)
            else:
                # TODO: fix convolution with non-equidistant
                #       convolution with levels
                # TODO: warn that xs_values is 2D and will be summed up
                values = misc.convolve(xs_wavelength, xs_values, wavelength, fwhm,
                                       mode='gauss')

        # TODO: amf


        # TODO: vertically integrate?
        levels = xs_levels


        spline = None

        if highpass:
            x = np.arange(values.size)
            values -= np.polyval(np.polyfit(x, values, 3), x)



        # scaling
        if isinstance(scaling, str):
            if scaling == 'ptp':
                scaling = 1.0 / np.ptp(values)
            elif scaling == 'max':
                scaling = 1.0 / np.max(values)
            else:
                raise NotImplementedError

        values *= scaling

        return cls(name=name, wavelength=wavelength, values=values,
                   levels=levels, scaling=scaling, start=start,
                   do_caching=False, spline=spline)




    def __call__(self, x, b):
        """\
        Compute optical depth using tau/cross section, amf and
        scaling x.

        If self.do_caching is True, tau is used from cached data and
        thus amf will be only used if p is different from last call.
        """
        if self.spline is None:
            x = x[self.mapping]
        else:
            # TODO
            b['V0'] /= self.scaling
            x = self.spline(x,b)

            if x.size != self.values.size:
                x = np.interp(self.wavelength, b['w0'], x)

        if np.ndim(self.values) == 1:
            values = self.values * x

        elif np.ndim(self.values) == 2:
            values = np.zeros(self.wavelength.shape)

            if 'amf' in b['data']:
                amf = b['data']['amf']
                values[self.nonzero] = x * np.dot(amf.values, self._nonzero_values2d)
            else:
                print('Warning: No boxAMF found!')
                values2d = self.values[:,self.nonzero]
                values[self.nonzero] = x * np.sum(values2d, axis=0)

        else:
            raise ValueError

        return values




class SplineDefinition:
    def __init__(self, name='spline', kind='cardinal', degree=-1,
                 n_subwindows=None, knots=None, knot_distance=10,
                 bands_or_size=None, start=0):
        """
        Class holding the definition of a spline.
        """
        self.bands_or_size = bands_or_size
        self.kind = kind
        self.degree = degree
        self.n_subwindows = n_subwindows
        self.knots = knots
        self.knot_distance = knot_distance
        self.name = name
        self.start = start

    def __repr__(self):
        return '%s name: %s, kind: %s, degree: %s' % (self.__class__,
                                                      self.name,
                                                      self.kind,
                                                      self.degree )


class Spline(Function):
    type_ = "Spline"

    def __init__(self, bands_or_size, kind='cardinal', degree=-1,
                 n_subwindows=None, start=0, knots=None, knot_distance=10,
                 name=None):
        """\
        A wrapper class around polynomial/spline fitting.
        After initlization, this class can be called (poly(x)) to return
        poly values.

        """
        self.name = str(name)
        self.kind = kind

        if isinstance(bands_or_size, int):
            self.size = bands_or_size
            self.bands = np.arange(self.size, dtype='f8')
        else:
            self.size = len(bands_or_size)
            self.bands = bands_or_size

        self.degree = degree

        if self.kind in ['B-spline', 'C-spline']:
            if knots is None:
                if n_subwindows is None:
                    n_subwindows = int(self.size / knot_distance + 0.5)

                self.knots = np.linspace(self.bands[0], self.bands[-1],
                                         n_subwindows + 1)
            else:
                self.knots = np.array(knots)

        elif self.kind == 'lagrange':
            self.knots = np.linspace(self.bands[0], self.bands[-1],
                                     self.degree + 1)

        else: # polynomial: cardinal
            self.knots = knots

        self.n_coefficients = misc.calculate_n_coefficients(kind, degree,
                                                            n_subwindows,
                                                            self.knots)

        self.start = start
        self.end = start + self.n_coefficients
        self.mapping = np.arange(self.start, self.end)

        if self.kind == 'B-spline':
            self.knots = np.concatenate([
                self.knots[0].repeat(self.degree),
                self.knots,
                self.knots[-1].repeat(self.degree)
            ])


    @classmethod
    def from_definition(cls, sdef, **kwargs):
        """
        Initilize from SplineDefinition class. Use kwargs to overwrite values
        in SplineDefinition class.
        """
        for name in ['bands_or_size', 'kind', 'degree', 'n_subwindows',
                     'knots', 'knot_distance', 'name', 'start']:
            if name not in kwargs:
                kwargs[name] = getattr(sdef, name)

        return cls(**kwargs)


    def fit(self, values):
        """\
        Returns first estimate of state vector.
        """
        if self.kind == 'B-spline':
            knots = self.knots[self.degree+1:-self.degree-1]
            tck, fp, ier, msg = scipy.interpolate.splrep(
                self.bands, values, t=knots, k=self.degree, full_output=1, s=0
            )
            coefficients = tck[1][:self.n_coefficients]

        elif self.kind == 'C-spline':
            if self.bands.size > 10000:
                coefficients = np.interp(self.knots, self.bands, values)

            x0 = np.full(self.knots.shape, values.mean(), dtype=float)
            coefficients = solver.linear_least_square(x0, values, self,
                                                      {'use_mapping': False})

        elif self.kind == 'lagrange':
            coefficients = np.interp(self.knots, self.bands, values)

        elif self.kind == 'cardinal':
            coefficients = np.polyfit(self.bands, values, self.degree)

        elif self.kind == 'scaling':
            coefficients = np.ones(self.n_coefficients)

        else:
            coefficients = np.array([])

        return coefficients


    def __call__(self, x, b=None):
        """\
        Calculate spline/polynomial for state vector x.
        """
        if b is None:
            b = {}

        if b.get('use_mapping', True):
            x = x[self.mapping]

        if self.kind == 'B-spline':
            tck = (self.knots, x, self.degree)
            values = scipy.interpolate.splev(self.bands, tck, ext=2)

        elif self.kind == 'C-spline':
            x = np.asarray(x, dtype='f8')
            bands = np.asarray(self.bands, dtype='f8')
            knots = np.asarray(self.knots, dtype='f8')

            values = cspline.compute_hermite_spline(bands, x, knots)

        elif self.kind == 'lagrange':
            # TODO: this is slow, rewrite using pre-calculated basis
            # with knots and bands
            values = scipy.interpolate.lagrange(self.knots, x)(self.bands)

        elif self.kind == 'cardinal':
            values = np.polyval(x, self.bands)

        elif self.kind == 'zeros':
            values = np.zeros(self.size)

        else:
            raise ValueError('Unknown kind "%s".' % self.kind)

        return values



class Ring(Function):
    type_ = 'Ring'

    def __init__(self, cw=None, fwhm=None, solar_filename=None, start=0,
        ring=None, spline=None):

        self.name = 'Ring'
        self.w = cw

        self.spline = spline
        if spline is None:
            self.mapping = np.array([start])
        else:
            self.mapping = np.arange(start, start+spline.n_coefficients)


        if ring is None: # TODO: compute ring cross section
            raise ValueError('Ring cross section `ring` not provided.')
        else:
            self.ring = ring

    @classmethod
    def from_file(cls, filename, start=0):
        cw, ring = np.loadtxt(filename, unpack=True)
        return cls(cw=cw, ring=ring, start=start)


    def __call__(self, x, b):
        if self.spline is None:
            x = x[self.mapping]
        else:
            x = self.spline(x,b)

        w0 = b.get('w0')
        ring = misc.interpolate(w0, self.w, x * self.ring)

        return ring


class Resolution(object):
    def __init__(self, w, E, cw, h, dh, isf='gauss', start=0, mode='log'):
        """\
        Resolution cross section for correcting differences between
        slit function parameters of reference and spectrum.
        """
        self.w = np.squeeze(w)
        self.E = np.squeeze(E)
        self.mode = mode

        n = self.E.shape[0] if np.ndim(self.E) == 2 else 1
        self.mapping = np.arange(start,start+n)

        if self.mapping.size == 1:
            self.resol = (
                misc.convolve(self.w, self.E, cw, h, isf) - misc.convolve(self.w, self.E, cw, h + dh, isf)
            ) / dh
            if self.mode == 'log':
                self.resol = self.resol / misc.convolve(self.w, self.E, cw, h, isf)
        else:
            self.resol = []
            for i in range(n):
                r = (misc.convolve(self.w, self.E[i], cw, h, isf) - misc.convolve(self.w, self.E[i], cw, h + dh, isf)) / dh

                if self.mode[i] == 'log':
                    r = r / misc.convolve(self.w, self.E[i], cw, h, isf)

                self.resol.append(r)


    def __call__(self, x, b):
        if self.mapping.size == 1:
            return x[self.mapping] * self.resol
        else:
            return np.dot(x[self.mapping], self.resol)




class Undersampling:
    def __init__(self, w, E, start=0, mode='log'):
        """\
        mode:
           - optical density mode (od)
        """
        self.name = 'USAMP'
        self.w = np.squeeze(w)
        self.E = np.squeeze(E)

        self.mode = mode

        n = self.E.shape[0] if np.ndim(self.E) == 2 else 1
        self.mapping = np.arange(start,start+n)
        self.n_coefficients = len(self.mapping)

    def __call__(self, x, b):

        # implicit slit correction
        cw0, fwhm0 = b['w0'], b['h0']
        cw, fwhm = b['w'], b['h']

        # spectrum cw is interpolated to refernce cw0
        if self.mapping.size == 1:

            over = misc.convolve(self.w, self.E, cw0, fwhm0)
            under = misc.interpolate(
                cw0, cw, misc.convolve(self.w, self.E, cw, fwhm),
                k=3
            )
            if self.mode == 'log':
                usamp = x[self.mapping[0]] * np.log(under / over)
            else:
                usamp = x[self.mapping[0]] * (over - under)

        else:
            usamp = np.zeros_like(cw0)

            for i in range(self.mapping.size):
                over = misc.convolve(self.w, self.E[i], cw0, fwhm0)
                under = misc.interpolate(
                    cw0, cw, misc.convolve(self.w, self.E[i], cw, fwhm),
                    k=3
                )
                if self.mode[i] == 'log':
                    usamp += x[self.mapping[i]] * np.log(under / over)
                else:
                    usamp += x[self.mapping[i]] * (over - under)

        return usamp





if __name__ == '__main__':
    pass

#! /usr/bin/env python
# coding: utf-8

# flexDOAS is a Python library for the development of DOAS
# algorithm.
# Copyright (C) 2016  Gerrit Kuhlmann (gerrit.kuhlmann@empa.ch)
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License a
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import numpy as np
import xarray


def read_cross_section(filename, filetype):
    """
    Read cross section from different file types.
    """
    if filetype == 'arts':
        with xarray.open_dataset(filename) as arts_file:
            w = arts_file['wvl'][:]
            z = 1e3 * arts_file['z'][::-1] # km -> m
            tau = arts_file['tau'][::-1,:,0,0]

    elif filetype == 'npy':
        w, tau = np.load(filename)
        z = np.nan

    elif filetype == 'ascii':
        w, tau = np.loadtxt(filename, unpack=True)
        z = np.nan

    return w,tau,z

if __name__ == '__main__':
    pass

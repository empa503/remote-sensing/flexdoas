#! /usr/bin/env python
# coding: utf-8

# flexDOAS is a Python library for the development of DOAS
# algorithm.
# Copyright (C) 2016  Gerrit Kuhlmann (gerrit.kuhlmann@empa.ch)
#
# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU General Public License a
# published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import warnings

import astropy.time
import astropy.coordinates
import numba
import numpy as np
import scipy
import scipy.interpolate

import xarray


def compute_solar_position(lon, lat, utc_time):
    """
    Compute solar position.

    """
    utc_time = astropy.time.Time(utc_time)
    loc = astropy.coordinates.EarthLocation.from_geodetic(lon=lon, lat=lat)

    # Altitude/Azimith coords: 0=North, 90=East
    altaz = astropy.coordinates.AltAz(obstime=utc_time, location=loc)

    sun = astropy.coordinates.get_sun(utc_time)
    sun = sun.transform_to(altaz)

    # convert azimuth: 0=South, West=90 (used by libradtran)
    saa = np.array(sun.az - 180.0 * astropy.units.degree)
    sza = np.array(sun.zen)

    return sza, saa




def calculate_n_coefficients(kind, degree, n_subwindows=None, knots=None):
    """\
    Calculate the number of coefficients for polynomials/splines.
    Returns 0 if kind not known.
    """
    if kind == 'B-spline':
        if n_subwindows is None:
            return knots.size + degree + 1
        else:
            return (n_subwindows + 1) + (degree + 1)
    elif kind == 'C-spline':
        if n_subwindows is None:
            return knots.size
        else:
            return n_subwindows + 1
    elif kind in ['cardinal', 'lagrange']:
        return degree + 1
    else:
        return 0



def slant_column_fmt(x, x_std):
    """\
    Format slant column plus error to be used with matplotlib.
    """
    b = np.floor(np.log10(np.abs(x))).astype(int)
    return r'%.3f$\pm$%.3f$\times$10$^{%d}$' % (x / 10.0**b, x_std / 10.0**b, b)


def compute_rms(values):
    """\
    Compute root-mean-square of values.
    """
    values = values[np.isfinite(values)]
    return np.sqrt(np.mean(values**2))


def create_covariance_matrix(sigma, knots, length):
    """\
    Create covariance matrix for spline.
    """
    if knots is None:
        n = 0
    else:
        n = knots.size

    Sa = np.empty((n,n))

    if np.ndim(sigma) == 0:
        sigma = np.full(n, sigma)

    if length is not None and np.ndim(length) == 0:
        length = np.full(n, length)

    for i,j in np.ndindex(n,n):
        if length is None:
            if i == j:
                Sa[i,j] = sigma[i] * sigma[j]
            else:
                Sa[i,j] = 0.0
        else:
            l = 0.5 * (length[i] + length[j])
            Sa[i,j] = sigma[i] * sigma[j] * np.exp(- abs(knots[i] - knots[j]) / l)

    return Sa



def interpolate(xp, x, y, out_type='float', k=1, ext=0):
    """\
    Wrapper around ``scipy.interpolate.UnivariateSpline`` useful
    for mapping bands to wavelengths and vice versa. If out_type
    is "index" returns integer.
    """
    if np.ndim(y) == 1:
        valid = np.isfinite(y)
        f = scipy.interpolate.UnivariateSpline(x[valid], y[valid], k=k, s=0, ext=ext)
        yp = f(xp)
    else:

        yp = np.empty((y.shape[0], xp.size))

        for i in range(y.shape[0]):
            valid = np.isfinite(y[i])
            f = scipy.interpolate.UnivariateSpline(x[valid], y[i,valid], k=k, s=0, ext=ext)
            yp[i,:] = f(xp)

    if out_type == 'index':
        return np.round(yp).astype(int)

    return yp


def centres(x):
    """\
    Convert level to layer by 0.5 * (x[1:] + x[:-1]).
    """
    return 0.5 * (x[1:] + x[:-1])


def sampling_interval(x):
    x = np.asarray(x)
    c = centres(x)

    c0 = x[0] - (c[0] - x[0])
    cn = x[-1] + (x[-1] - c[-1])

    return np.diff( np.concatenate([[c0], c, [cn]]) )



def convolve(x, y, cw, sfp, mode='gauss'):
    """\
    Convolve x,y with cw,fwhm for slit function (mode):
    - gauss (Gaussian)
    - erf (Error Function)
    - asym_gauss (Asymetric Gaussian)
    """
    x = np.asarray(x, dtype='f8')
    y = np.asarray(y, dtype='f8')
    cw = np.asarray(cw, dtype='f8')
    sfp = np.asarray(sfp, dtype='f8')

    if np.std(np.diff(x)) / np.mean(np.diff(x)) > 1e-9:
        print(np.std(np.diff(x)), np.mean(np.diff(x)))
        raise ValueError('x need to be equidistant!')

    return _convolve(x, y, cw, sfp, mode)


def _convolve(x, y, cw, sfp, mode):

    sigma = fwhm2sigma(sfp)

    # this assumes equidistant grid
    dx = x[1] - x[0]
    a = dx / np.sqrt(2.0 * np.pi)

    values = np.zeros(cw.size)

    imin = np.array((cw - 5.0 * sigma - x[0]) / dx, dtype=int)
    imin[imin < 0] = 0

    imax = np.array((cw + 5.0 * sigma - x[0]) / dx + 1, dtype=int)
    imax[imax > x.size] = x.size

    for i in range(cw.size):
        j, k = imin[i], imax[i]

        if j <= k:
            if mode == 'gauss':
                g = a / sigma[i] * np.exp(-(x[j:k] - cw[i])**2 / (2.0 * sigma[i]**2))
            else:
                raise ValueError

            values[i] = np.dot(y[j:k], g)
        else:
            values[i] = 0.0

    return values




def convolve_gaussian(x, y, cw, fwhm, integrate=False, points=10):
    """\
    Convolve x,y with cw,fwhm with x not being equidistant.
    """
    ssi = sampling_interval(cw)

    z = []
    for i in range(cw.size):
        wmin = cw[i] - 0.5 * ssi[i]
        wmax = cw[i] + 0.5 * ssi[i]

        w = np.linspace(wmin,wmax,points)
        h = interpolate(w, cw, fwhm, k=3)

        zi = np.mean(_convolve(x, y, w, h, 'gauss'))
        z.append(zi)

    return np.array(z)




def refractive_index_of_air(w=None, p=None, t=None, e=None):
    """\
    Computes the refractive index of air for given wavelength (w/nm) and,
    if given, includes corrections for atmospheric pressure (p/Pa),
    temperature (t/C) and partial water vapour pressure (e/hPa).

    References:
    Birch and Downs: Updated Edlen Equation for the Refractive Index of Air, 1993
    """
    w = 1e-3 * w # nm -> um

    n = 1 + 1e-8 * (8343.05 + 2406294.0 / (130 - w**-2) + 15999.0 / (38.9 - w**-2))

    if p is not None and t is not None:
        raise NotImplementedError

    if e is not None:
        raise NotImplementedError

    return n


def vac2air_wavelength(wvl):
    return wvl / refractive_index_of_air(wvl)

def air2vac_wavelength(wvl):
    return wvl * refractive_index_of_air(wvl)




@numba.jit(nopython=True)
def _geometric_boxamfs(z, sza, vza, elevation, altitude):

    shape = (z.size - 1,) + sza.shape
    bamf = np.empty(shape)

    for k in range(z.size - 1):
        zmin = z[k]
        zmax = z[k+1]

        for i,j in np.ndindex(sza.shape):
            mu0 = 1.0 / np.cos(np.deg2rad(sza[i,j]))
            mu  = 1.0 / np.cos(np.deg2rad(vza[i,j]))

            if zmax <= elevation[i,j]:
                bamf[k,i,j] = 0.0

            elif zmin <= elevation[i,j] < zmax:
                f = 1.0 - (elevation[i,j] - zmin) / (zmax - zmin)
                bamf[k,i,j] = f * (mu0 + mu)

            elif zmin <= altitude[i,j] < zmax:
                f = (altitude[i,j] - zmin) / (zmax - zmin)
                bamf[k,i,j] = mu0 + f * mu

            elif zmin > altitude[i,j]:
                bamf[k,i,j] = mu0

            else:
                bamf[k,i,j] = mu + mu0

    return bamf



def geometric_boxamfs(z, sza, vza, elevation, altitude):
    """\
    Compute geometric layer AMFs.

    z: vertical levels heights (ground to TOA) [m]

    sza: solar zenith angle [deg]
    vza: viewing zenith angle [deg]

    elevation: ground elevation [m]
    altitude: aircraft or satellite altitude [m]
    """
    assert np.all(np.diff(z) > 0.0)

    shape = (z.size-1, ) + sza.shape
    dims = ('layers', ) + sza.dims
    bamf = xarray.DataArray(np.empty(shape), coords=sza.coords, dims=dims)

    z = np.asarray(z)
    sza = np.asarray(sza)
    vza = np.asarray(vza)
    elevation = np.asarray(elevation)
    altitude = np.asarray(altitude)

    bamf[:] = _geometric_boxamfs(z, sza, vza, elevation, altitude)

    return bamf




def fwhm2sigma(fwhm):
    return fwhm / (2.0 * np.sqrt(2.0 * np.log(2.0)))

def sigma2fwhm(sigma):
    return sigma * (2.0 * np.sqrt(2.0 * np.log(2.0)))

